import importlib
import types

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


class ModuleReloadFailureHandler:
    def __init__(self, module: types.ModuleType):
        self.module = module

    def handle_error(self, error: Exception):
        print("Module '" + self.module.__name__ + "' reloading failure: " + str(error))


class _EventHandler(FileSystemEventHandler):
    def __init__(self, module: types.ModuleType, failure_handler: ModuleReloadFailureHandler):
        self.__module = module
        self.__module_path = module.__file__
        self.__exists = True
        self.__was_recently_reloaded = False
        self.__failure_handler = failure_handler

    def on_created(self, event):
        if event.src_path == self.__module_path:
            self.__exists = True

    def on_deleted(self, event):
        if event.src_path == self.__module_path:
            self.__exists = False

    def on_modified(self, event):
        if not self.__exists:
            return

        if event.src_path == self.__module_path:
            try:
                importlib.reload(self.__module)
                self.__was_recently_reloaded = True
            except Exception as error:
                self.__failure_handler.handle_error(error)

    def was_recently_reloaded(self):
        result = self.__was_recently_reloaded
        self.__was_recently_reloaded = False
        return result


class ModuleReloader:
    def __init__(self, module: types.ModuleType, failure_handler: ModuleReloadFailureHandler=None):
        if failure_handler is None:
            failure_handler = ModuleReloadFailureHandler(module)

        module_dir = "/".join(module.__file__.split("/")[:-1])

        self.__event_handler = _EventHandler(module, failure_handler)
        self.__observer = Observer()
        self.__observer.schedule(self.__event_handler, module_dir)
        self.__observer.start()

    def was_recently_reloaded(self):
        return self.__event_handler.was_recently_reloaded()

    def __del__(self):
        self.__observer.stop()

